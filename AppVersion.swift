//
//  AppVersion.swift
//  
//
//  Created by Andrew Benson on 6/13/19.
//  Copyright © 2019 Nuclear Cyborg Corp. All rights reserved.
//
import Foundation

public struct AppVersion: Codable {
    public static var current: AppVersion {
        let appVersionNumber = AppVersionComponent(Bundle.main.infoDictionary!["CFBundleShortVersionString"]! as! String)
        let appBuildNumber   = AppVersionComponent(Bundle.main.infoDictionary!["CFBundleVersion"]! as! String)

        return AppVersion(version: appVersionNumber, build: appBuildNumber)
    }

    public let version: AppVersionComponent
    public let build: AppVersionComponent

    public init(version: AppVersionComponent, build: AppVersionComponent) {
        self.version = version
        self.build = build
    }
}

extension AppVersion: CustomStringConvertible {
    public var description: String {
        return "\(version)(\(build))"
    }
}

extension AppVersion: Comparable {
    public static func < (lhs: AppVersion, rhs: AppVersion) -> Bool {
        if lhs.version < rhs.version {
            return true
        } else if lhs.version > rhs.version {
            return false
        } else if lhs.build < rhs.build {
            return true
        } else {
            return false
        }
    }
}

public struct AppVersionComponent: Codable {
    public let components: [Int]

    public init(_ version: String) {
        var comps = version.components(separatedBy: ".")

        // Ensure there are at least 4 components
        while comps.count < 4 {
            comps.append("0")
        }
        components = comps.map({ (s) -> Int in
            Int(s) ?? 0
        })
    }
}

// MARK: - CustomStringConvertible
extension AppVersionComponent: CustomStringConvertible {
    public var description: String {
        return components.map({ (component) -> String in
            "\(component)"
        }).joined(separator: ".")
    }
}

// MARK: - Equatable
extension AppVersionComponent: Equatable {
    public static func == (lhs: AppVersionComponent, rhs: AppVersionComponent) -> Bool {
        let lhsComponents = lhs.components
        let rhsComponents = rhs.components
        let count = lhsComponents.count
        precondition(count == rhsComponents.count,
                     "AppVersionComponent: Equatable: Both lhs and rhs must have an equal number of components")

        for i in 0 ..< count {
            let left = lhsComponents[i]
            let right = rhsComponents[i]
            if left != right {
                return false
            }
        }

        return true
    }
}

// MARK: - Comparable
extension AppVersionComponent: Comparable {
    public static func < (lhs: AppVersionComponent, rhs: AppVersionComponent) -> Bool {
        let lhsComponents = lhs.components
        let rhsComponents = rhs.components
        let count = lhsComponents.count
        precondition(count == rhsComponents.count,
                     "AppVersionComponent: Comparable: Both lhs and rhs must have an equal number of components")

        // Compare each component until a difference is found
        for i in 0 ..< count {
            let left = lhsComponents[i]
            let right = rhsComponents[i]
            if left < right {
                return true
            } else if left > right {
                return false
            }
        }
        return false
    }
}

